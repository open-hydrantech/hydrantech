
import React from 'react';
import { NavigationActions, addNavigationHelpers, TabNavigator, StackNavigator } from 'react-navigation';
import { inject, observer } from 'mobx-react/native';
import Login from '../view/screens/Login/Login';
import Loading from '../view/components/Loading/Loading';
import AuthRouter from './AuthRouter';

const L = inject('Store')(observer((props) => {
	props.Store.nav.setAppNavigation(props.navigation);
	return (<Loading loading />);
}));

const AppRouter = StackNavigator(
	{
		Loading: { screen: L },
		Auth: { screen: AuthRouter },
		Login: { screen: Login },
	},
	{
		headerMode: 'none',
		mode: 'modal',
		initialRouteName: 'Loading',
		gesturesEnabled: false
	}
);

export default AppRouter;
