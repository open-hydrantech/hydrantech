
import React from 'react';
import { addNavigationHelpers, TabNavigator, StackNavigator } from 'react-navigation';
import { inject, observer } from 'mobx-react/native';
import _ from 'lodash';

import List from '../view/screens/Auth/List/List';
import Profile from '../view/screens/Auth/Profile/Profile';

const L = inject('Store')(observer((props) => {
	props.Store.nav.setAuthNavigation(props.navigation);
	return (<List />);
}));

const AuthRouter = TabNavigator(
	{
		Profile: {
			screen: Profile,
			navigationOptions: {
				title: 'פרופיל',
			},
		},
		List: {
			screen: L,
			navigationOptions: ({ navigation: { state } }) => ({
				title: (state.params && state.params.title) || 'אירועים'
			}),
		},
	},
	{
		initialRouteName: 'List',
		tabBarOptions: {
			activeTintColor: '#c63644',
			inactiveTintColor: '#e91e63',
			labelStyle: {
				fontSize: 18,
				fontWeight: '300',
			},
			style: {
				backgroundColor: 'aliceblue',
			},
			indicatorStyle: {
				backgroundColor: '#c63644',
			},
		}
	}
);


export default AuthRouter;
