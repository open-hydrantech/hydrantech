import {
	spy,
	intercept,
	extendObservable,
	observe,
	autorun,
	reaction,
	when,
	toJS,
	useStrict,
	observable,
	action,
	computed,
} from 'mobx'

import NavStore from './NavStore'
import EventsStore from './EventsStore'
import AppStore from './AppStore'

useStrict(true)

class Store {
	@observable events

	constructor() {
		console.log('mounted store')
		this.app = new AppStore(this)
		this.nav = new NavStore(this)

		reaction(
			() => this.nav.authNavigation && this.app.auth,
			auth => {
				if (auth) {
					this.events = new EventsStore(this)
				} else {
					delete this.events
				}
			},
		)
	}
}

const store = new Store()

export default store
