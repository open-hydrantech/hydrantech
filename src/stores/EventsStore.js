import FCM, { FCMEvent } from 'react-native-fcm'
import _ from 'lodash'
import { reaction, useStrict, observable, action, computed } from 'mobx'
import openMap from '../lib/map'
import * as u from '../lib/Utils'
import Settings from '../config/settings'

useStrict(true)

class Store {
	@action s = s => _.assign(this, s)
	@observable data = []

	async init() {
		this.notificationListener = FCM.on(FCMEvent.Notification, notif => notif)
		this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, async fcmToken => {
			this.root.app.setFcmToken({ fcmToken })
		})
		const fcmToken = await FCM.getFCMToken()
		console.log('the one -fcmToken', fcmToken)
		this.root.app.setFcmToken({ fcmToken })
	}
	constructor(root) {
		this.root = root

		this.init()

		this.loadStore().then(() => {
			this.runFetchEvents()
		})
		reaction(
			() => _.filter(this.data, 'isNew').length,
			n => this.root.nav.authNavigation.navigate('List', { title: (n ? `(${n})` : '') + 'אירועים' }),
		)
	}

	@computed
	get dataArray() {
		return this.data.peek()
	}
	@action
	replaceData(d) {
		this.data.replace(d)
	}
	@action
	clearData() {
		this.data.clear()
	}
	@action
	assignData(obj) {
		u.arrayAssign(this.data, obj)
	}
	@action
	assignItem(item, obj) {
		_.assign(item, obj)
	}
	@action
	pushNewData = newData => {
		_.forEachRight(newData, el => this.data.unshift({ ...el, isNew: true }))
		this.saveData()
	}
	saveData = async () => u.LocalStorage.save('data', this.data)

	@action
	async onItemPressed(index) {
		await u.sleep(100)
		const item = this.data[index]
		const { lat, lon, address } = item
		if (lat && lon) {
			openMap({ lat, lon, address: address || 'כתובת התקנה' })
		}
		if (item.isNew) {
			this.assignItem(item, { isNew: false })
			await u.sleep(100)
			this.saveData()
		}
	}

	@action
	async loadStore() {
		const eventsUser = await u.LocalStorage.get('eventsUser')
		if (eventsUser && eventsUser.userId == this.root.app.user.userId) {
			this.replaceData((await u.LocalStorage.get('data')) || [])
		} else {
			u.LocalStorage.save('eventsUser', this.root.app.user)
			this.clearData()
		}
	}
	@action
	async toggleAllDataNew() {
		await u.sleep(100)
		this.assignData({ isNew: !_.some(this.data, 'isNew') })
		this.saveData()
	}

	@action
	async runFetchEvents() {
		console.log('processFetchEvents')
		const enterDate = u.now()

		const { user } = this.root.app
		const createdAt = this.data.length ? _.get(this.data, '[0].createdAt', 0) : 0
		let { data } = await u.toto({
			timeOut: 15000,
			promise: u.meteorCall('mobile.get.events', { user, createdAt, appName: Settings.APP_NAME }),
		})
		if (data) this.pushNewData(data)

		const spent = u.now() - enterDate
		setTimeout(() => this.runFetchEvents(), spent > 4000 ? 1000 : 5000 - spent)
	}
}

export default Store
