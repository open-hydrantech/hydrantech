
import React from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'mobx-react';
import AppRouter from '../router/AppRouter';
import Store from '../stores/Store';

const App = () => {
	console.log('rendering app');
	return (
		<Provider {...{ Store }}>
			<AppRouter />
		</Provider>
	);
};

export default App;
