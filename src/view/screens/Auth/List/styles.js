import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	data: {
		fontSize: 14,
		color: '#333333',
		fontWeight: '500',
	},
	dataNew: {
		fontSize: 15,
		color: '#c63644',
		fontWeight: '500',
	},
	header: {
		fontSize: 13,
		color: 'black',
		fontWeight: '100',
	},
	headerNew: {
		fontSize: 13,
		color: 'black',
		fontWeight: '100',
	},
});

export default styles;
