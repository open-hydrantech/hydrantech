export default {
	APP_NAME: 'HydranTech',
	SERVER_URL: 'wss://app.hydrant-system.com/websocket',
};
